#define LED 13

#define SONAR_TRIGGER A5
#define SONAR_ECHO    A4

#define STBY 8
#define PWMA 3
#define AIN2 2
#define AIN1 4
#define PWMB 5
#define BIN1 7
#define BIN2 6

void setup()
{
  // Define inputs and outputs
  pinMode(SONAR_TRIGGER, OUTPUT);
  pinMode(SONAR_ECHO,    INPUT);

  pinMode (LED,  OUTPUT);
  pinMode (STBY, OUTPUT);
  pinMode (PWMA, OUTPUT);
  pinMode (AIN2, OUTPUT);
  pinMode (AIN1, OUTPUT);
  pinMode (PWMB, OUTPUT);
  pinMode (BIN1, OUTPUT);
  pinMode (BIN2, OUTPUT);
  
  digitalWrite (STBY, HIGH);
  digitalWrite (AIN2, HIGH);
  digitalWrite (AIN1, LOW);
  digitalWrite (BIN2, LOW);
  digitalWrite (BIN1, HIGH);

  Serial.begin (9600);
}

long
get_distance ()
{
  digitalWrite(SONAR_TRIGGER, LOW);
  delayMicroseconds(5);
  digitalWrite(SONAR_TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(SONAR_TRIGGER, LOW);

  long duration = pulseIn(SONAR_ECHO, HIGH);
  long distance = (duration/2) / 29.1; // cm

  Serial.println ("DISTANCE:"); Serial.println (distance); Serial.println ("\n");

  return distance;
}

struct MotorsInput {
  int rotation;
  float speed;
};

void
set_motors (MotorsInput *motors_input, long distance)
{
  if (distance >= 20)
  {
    motors_input->rotation = 0;
    motors_input->speed = 50;
  }
  else if (distance < 7)
  {
    if (motors_input->rotation == 0)
    {
      motors_input->rotation = rand () & 1 ? -1 : 1;
    }
    motors_input->speed = -50;
  }
  else if (distance < 20)
  {
    motors_input->rotation = 0;
    motors_input->speed = 20;
  }
  else
  {
    motors_input->rotation = 0;
    motors_input->speed = 0;
  }
}

void
loop()
{
  static MotorsInput motors_input = {};
  long distance = get_distance ();
  set_motors (&motors_input, distance);

  Serial.print("d:"); Serial.print(distance);
  Serial.print("s:"); Serial.print(motors_input.speed);
  Serial.print("r:"); Serial.print(motors_input.rotation);
  Serial.print("\n");

  if (motors_input.speed > 255)
  {
    motors_input.speed = 255; 
  }
  else if (motors_input.speed < -255)
  {
    motors_input.speed = -255;
  }
    
  if (motors_input.speed > 10)
    {
      digitalWrite (STBY, HIGH);
      digitalWrite (AIN1, LOW);
      digitalWrite (AIN2, HIGH);
      digitalWrite (BIN1, HIGH);
      digitalWrite (BIN2, LOW);
      analogWrite (PWMA, motors_input.speed);
      analogWrite (PWMB, motors_input.speed);
    }
  else if (motors_input.speed < -10)
    {
      digitalWrite (STBY, HIGH);

      if (motors_input.rotation == 1)
        {
          digitalWrite (AIN1, HIGH);
          digitalWrite (AIN2, LOW);
          digitalWrite (BIN1, HIGH);
          digitalWrite (BIN2, LOW);
        }
      else if (motors_input.rotation == -1)
        {
          digitalWrite (AIN1, LOW);
          digitalWrite (AIN2, HIGH);
          digitalWrite (BIN1, LOW);
          digitalWrite (BIN2, HIGH);          
        }
      analogWrite (PWMA, -motors_input.speed);
      analogWrite (PWMB, -motors_input.speed);
    }
  else
    {
      digitalWrite (STBY, LOW);
    }
}

